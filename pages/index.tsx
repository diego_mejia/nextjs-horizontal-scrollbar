import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useEffect, useRef, useState } from "react";

export default function Home() {
  const heightRef = useRef<any>(null);
  const trackRef = useRef(null);
  const firstPhotoContainer = useRef(null);
  const messagePanel = useRef(null);
  const collagePanel = useRef(null);
  gsap.registerPlugin(ScrollTrigger);

  const setTrackHeight = () => {
    const allValues = Array.from(document.querySelectorAll(".inside-panel"));
    let tempHeight = allValues.reduce((accumulated, current) => {
      accumulated += current.getBoundingClientRect().width;
      return accumulated;
    }, 0);

    heightRef.current.style.height = tempHeight + "px";
  };

  let tlMain = null;

  useEffect(() => {
    setTrackHeight();
  }, []);

  useEffect(() => {
    tlMain = gsap
      .timeline({
        scrollTrigger: {
          trigger: heightRef.current,
          start: "top top",
          end: "bottom bottom",
          scrub: 1,
        },
      })
      .to(trackRef.current, {
        xPercent: -203,
        ease: "none",
      });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: firstPhotoContainer.current,
          start: "left left",
          end: "right left",
          scrub: true,
          containerAnimation: tlMain,
        },
      })
      .to(".pet", { scale: 1.3 });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: heightRef.current,
          start: "top top",
          end: "bottom bottom",
          scrub: 1,
        },
      })
      .to(".mini_loader_content", { width: "100%" });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: heightRef.current,
          start: "top top",
          end: "bottom bottom",
          scrub: 1,
        },
      })
      .to(".first_pet_shadow_box", { opacity: 0.9 });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: messagePanel.current,
          start: "left left",
          end: "10%",
          scrub: true,
          containerAnimation: tlMain,
        },
      })
      .to(".message", {
        marginBottom: 10,
        fontSize: "5rem",
        letterSpacing: "10px",
      });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: messagePanel.current,
          start: "left left",
          end: "right 80%",
          scrub: true,
          containerAnimation: tlMain,
        },
      })
      .to(".message_panel_top", {
        clipPath: "polygon(1% 0, 0 0, 0 100%, 1% 100%)",
        // xPercent: -100,
        ease: 'none'
      });

    gsap
      .timeline({
        scrollTrigger: {
          trigger: collagePanel.current,
          start: "left right",
          end: "right left",
          scrub: true,
          containerAnimation: tlMain,
        },
      })
      .to(".collage_panel_img", { x: "10vw", stagger: 0.05 });
  }, []);

  return (
    <div className={styles.main_wrapper}>
      <div className={styles.hero}>
        <h2>photographer&apos;s</h2>
        <h1>Celebration</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi
          accusamus quam dolore corrupti dicta repellendus expedita unde. Nisi
          culpa dolores, architecto totam vitae, aperiam eveniet quibusdam error
          dignissimos ipsa sequi.
        </p>
      </div>
      <div
        className={`${styles.section_height} section_height`}
        ref={heightRef}
      >
        <div className={styles.sticky_element}>
          <div className={`${styles.mini_loader} mini_loader`}>
            <div
              className={`${styles.mini_loader_content} mini_loader_content`}
            ></div>
          </div>

          <div className={styles.track} ref={trackRef}>
            {/*
             * FIRST PANEL
             */}
            <div
              className={`${styles.first_pet} inside-panel`}
              ref={firstPhotoContainer}
            >
              <div
                className={`${styles.first_pet_shadow_box} first_pet_shadow_box`}
              ></div>
              <Image
                className={`${styles.pet} pet`}
                alt="Erizo 1"
                style={{ objectFit: "cover" }}
                fill
                src="https://nypost.com/wp-content/uploads/sites/2/2021/05/hedgehog.jpg"
              />
            </div>

            {/*
             * SECOND PANEL
             */}
            <div
              className={`${styles.message_panel} inside-panel`}
              ref={messagePanel}
            >
              <div
                className={`${styles.message_panel_inside} ${styles.message_panel_top} message_panel_top`}
              >
                <p className="message">Special Edition</p>

                <Image
                  alt="Erizo 2"
                  width={400}
                  height={400}
                  src="https://th-thumbnailer.cdn-si-edu.com/qhn4f5uDwytUEKNehOauJNRLKTs=/1072x720/filters:no_upscale()/https://tf-cmsv2-smithsonianmag-media.s3.amazonaws.com/filer/ef/ff/efff9ae5-1832-489f-bb1f-f1a00944a8aa/hedgehog.jpg"
                />
              </div>

              <div
                className={`${styles.message_panel_inside} ${styles.message_panel_bottom} message_panel_bottom`}
              >
                <p className="message">Special Edition</p>
                <Image
                  alt="Erizo 2"
                  width={400}
                  height={400}
                  src="https://i.pinimg.com/736x/98/ba/48/98ba48c230f378e064a02ec15c3b7227.jpg"
                />
              </div>
            </div>
            <div
              className={`${styles.collage_panel} inside-panel`}
              ref={collagePanel}
            >
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_1} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://imageio.forbes.com/blogs-images/chrismyers/files/2018/08/rsz_liudmyla-denysiuk-779689-unsplash_cropped.jpg?format=jpg&width=960"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_2} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://static.wixstatic.com/media/nsplsh_6b5f453744705667667477~mv2_d_5259_3702_s_4_2.jpg/v1/crop/x_0,y_200,w_5259,h_3301/fill/w_560,h_430,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/Image%20by%20Siem%20van%20Woerkom.jpg"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_3} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://image.winudf.com/v2/image1/Y29tLmhlZGdlaG9nLndhbGxwYXBlci5mcmVlaGRpbWFnZXMuaGVkZ2Vob2dwaG90by5iZXN0cGljdHVyZV9zY3JlZW5fMF8xNTQ3MjA3NzY4XzA1NQ/screen-0.jpg?fakeurl=1&type=.webp"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_4} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://media-be.chewy.com/wp-content/uploads/2019/10/22152634/hedgehog-ball-1024x576.jpg"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_5} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://ichef.bbci.co.uk/images/ic/1040x1040/p06bn4bw.jpg"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_6} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://www.mrpets.ca/media/catalog/product/cache/361e5aa3eae0788fba8f53d174389d53/h/e/hedgehog_2.jpeg"
              />
              <Image
                className={`${styles.collage_panel_img} ${styles.collage_7} collage_panel_img`}
                alt="Erizo 3"
                width={300}
                height={300}
                src="https://wjla.com/resources/media2/16x9/full/1015/center/80/395356a9-bc60-439b-bfb5-bada3233ca3d-large16x9_Untitleddesign20220824T100231.008.png"
              />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.footer}>
        <h1>the end</h1>
      </div>
    </div>
  );
}
