/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'nypost.com'
      },
      {
        protocol: 'https',
        hostname: 'i.pinimg.com'
      },
      {
        protocol: 'https',
        hostname: 'wjla.com'
      },
      {
        protocol: 'https',
        hostname: 'static.wixstatic.com'
      },
      {
        protocol: 'https',
        hostname: 'th-thumbnailer.cdn-si-edu.com'
      },
      {
        protocol: 'https',
        hostname: 'image.winudf.com'
      },
      {
        protocol: 'https',
        hostname: 'ichef.bbci.co.uk'
      },
      {
        protocol: 'https',
        hostname: 'www.mrpets.ca'
      },
      {
        protocol: 'https',
        hostname: 'imageio.forbes.com'
      },
      {
        protocol: 'https',
        hostname: 'media-be.chewy.com'
      }
    ]
  }
}

module.exports = nextConfig
